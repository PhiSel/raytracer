#include <unittest++/UnitTest++.h>
#include "sdfloader.hpp"


SUITE(test_class_window)
{
  TEST(should_print_out_the_pic)
  {

  }
}

SUITE(test_class_SDF_loader)
{
  TEST(read_sdf_file)
  {
    SDFloader sdf;
    #if 1
      sdf.load("../sdf/example_jonas.sdf");
    #else
      sdf.load("../sdf/materialien.sdf");
    #endif

      sdf.print();
  }

  TEST(material_should_be_blue_with_0_0_1__0_0_1__0_0_1__10)
  {
    SDFloader sdf;
    sdf.load("../sdf/example.sdf");
    auto mat = sdf.material();

    auto clr_blue = mat.find("blue") -> second;

    CHECK_EQUAL(0, clr_blue.ka().r);
    CHECK_EQUAL(0, clr_blue.ka().g);
    CHECK_EQUAL(1, clr_blue.ka().b);

    CHECK_EQUAL(0, clr_blue.kd().r);
    CHECK_EQUAL(0, clr_blue.kd().g);
    CHECK_EQUAL(1, clr_blue.kd().b);

    CHECK_EQUAL(0, clr_blue.ks().r);
    CHECK_EQUAL(0, clr_blue.ks().g);
    CHECK_EQUAL(1, clr_blue.ks().b);

    CHECK_EQUAL(10, clr_blue.m());
  }

  TEST(material_should_be_red_with_1_0_0__1_0_0__1_0_0__10)
  {
    SDFloader sdf;
    sdf.load("../sdf/example.sdf");
    auto mat = sdf.material();

    auto clr_red = mat.find("red") -> second;

    CHECK_EQUAL(1, clr_red.ka().r);
    CHECK_EQUAL(0, clr_red.ka().g);
    CHECK_EQUAL(0, clr_red.ka().b);

    CHECK_EQUAL(1, clr_red.kd().r);
    CHECK_EQUAL(0, clr_red.kd().g);
    CHECK_EQUAL(0, clr_red.kd().b);

    CHECK_EQUAL(1, clr_red.ks().r);
    CHECK_EQUAL(0, clr_red.ks().g);
    CHECK_EQUAL(0, clr_red.ks().b);

    CHECK_EQUAL(10, clr_red.m());
  }

  TEST(bsphere_should_be_0_0_minus_100_50_blue)
  {
    SDFloader sdf;
    sdf.load("../sdf/example.sdf");
    auto objects = sdf.object();
    for(auto const& item : objects)
    {
      if (item -> name() == "bsphere")
      {
        CHECK_EQUAL("blue", item -> material().name());
      }
    }
  }

}

SUITE(test_class_material)
{

}

int main(int argc, char const *argv[])
{
  return UnitTest::RunAllTests();
}