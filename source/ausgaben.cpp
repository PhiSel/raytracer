#include "sdfloader.hpp"
#include "camera.hpp"
#include "scene.hpp"


int main(int argc, char const *argv[])
{

  SDFloader sdf;
// #if 1
//   sdf.load("../sdf/example.sdf");
// #else
//   sdf.load("../sdf/materialien.sdf");
// #endif
  
  Material mat{"Test", {1,2,3},{11,22,33}, {111,222,333}, 4};

  Ray r1{{0,0,0},{-0.5,-0.5,-0.5}};
  Ray r2{{0,0,0},{0.3,0.2,0.1}};
  Ray r3{{0,0,0},{2,2,2}}; 
  Ray r4{{1,1,1},{2,2,2}}; 

  Sphere s1{"s1",{1,1,1}, 0.5, mat};

  std::cout << "r1: bool: " << s1.intersect(r1).hitSomething << " t= " << s1.intersect(r1).rayParameter << "\n"; // Schneidet nicht, Kugel liegt hinter der Lichtquelle 
  std::cout << "r2: bool: " << s1.intersect(r2).hitSomething << " t= " << s1.intersect(r2).rayParameter << "\n"; // Schneidet nicht, Strahl zieht an Kugel vorbei
  std::cout << "r3: bool: " << s1.intersect(r3).hitSomething << " t= " << s1.intersect(r3).rayParameter << "\n"; // Schneidet, Ursprung ist außerhalb von Kreis
  std::cout << "r4: bool: " << s1.intersect(r4).hitSomething << " t= " << s1.intersect(r4).rayParameter << "\n"; // Schneidet, Ursprung ist im Kreis


  return 0;
}