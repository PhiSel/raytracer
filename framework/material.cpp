#include "material.hpp"

std::string Material::name() const
{
  return name_;
}

Color Material::ka() const
{
  return ka_;
}

Color Material::kd() const
{
  return kd_;
}

Color Material::ks() const
{
  return ks_;
}

float Material::m() const
{
  return m_;
}

std::ostream& operator <<(std::ostream& os, Material const& mat)
{
  os << "name: " << mat.name()
     << "\n ka: " << mat.ka()
     << "\n kd: " << mat.kd()
     << "\n ks: " << mat.ks()
     << "\n m: " << mat.m() << "\n";
  return os; 
}