#include "composite.hpp"

HitPoint Composite::intersect(Ray const& ray) const
{
  for(auto const& item : compy_)
  {
    auto temp = item -> intersect(ray);
    if (temp.hitSomething) { return temp; }
  }

  return {false, 0};
}

void Composite::add(std::shared_ptr<Shape> const& s)
{
  compy_.push_back(s);
}

std::ostream& Composite::print(std::ostream& os) const 
{
  Shape::print(os);
  os << " size: " << compy_.size()
     << "\n objects: ";
  for(auto const& item : compy_)
  {
    os << item -> name() << " ";
  }
  return os; 
} 