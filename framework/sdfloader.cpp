#include "sdfloader.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include "color.hpp"

Scene SDFloader::get_scene() const
{
  return scene_;
}


/************************************
* ------------- load() --------------
*************************************
* Laden einer beliebiger SDF-Datei,
* welche Zeile für Zeile eingelesen
* und in classify() weiter 
* verarbeitet wird
*************************************/

void SDFloader::load(std::string const& filename)
{
  std::ifstream file;     // Erstellt einen Inputstream ...
  file.open(filename);    // und öffnet die angegebene sdf-Datei. 

  if (file.is_open())     // Wenn die Datei geöffnet wurde ...
  { 
    while(file.good())    // und solange keine "error state flags" gesetzt sind ...
    {
     std::string getline;                      // Erstelle einen neuen String für eine Zeile
     std::getline(file, getline);              // und befülle ihn mit der obersten Zeile aus der Datei.

     std::stringstream line_stream(getline);   // Erstelle einen neuen stringstream aus der einzelnen Zeile ...
     classify(line_stream);                    // und übergebe ihn an classify
    }
  }
  else                    // Wurde die Datei nicht geöffnet, so erscheint eine Fehlermeldung
  {
    std::cout <<"Error 404 - file doesn't exist" <<std::endl;
  }

  file.close();          // Zum Schluss schließe die sdf-Datei.

  merge_transforms();
}


/************************************
* ------ material() & object() ------
*************************************
* Getter für den Zugriff der UnitTest
*************************************/

std::map<std::string, Material> SDFloader::material() const
{
  return scene_.mat_;
}


std::vector< std::shared_ptr<Shape> >SDFloader::object() const
{
  return scene_.obj_;
}


/*************************************
* ------------ classify() ------------
**************************************
* Klassifiziert zwischen den Typen:
* define, transform, render und 
* übergibt den Reststring an die
* entsprechende Methode
**************************************/

void SDFloader::classify(std::stringstream& line_stream)
{
  std::string command;
  line_stream >> command;
  if (command == "define")
  {
    what_define(line_stream);
  }
  else if (command == "transform")
  {
    what_transform(line_stream);
  }
  else if (command == "render")
  {
    // std::cout <<"yeah, it's a render-process\n";
  }
}


/**************************************
* ----------- what_define() -----------
***************************************
* Bekommt den Reststring von classify()
* und unterscheidet zwischen den 
* verschiedenen Definitionen:
* material, shape (mit Unterpunkten),
* light oder camera. 
* Der Reststring wird an die jeweilige
* Convert-Methode übergeben.
**************************************/


void SDFloader::what_define(std::stringstream& line_stream)
{
  std::string command;
  line_stream >> command;
  if (command == "material") { convert_material(line_stream); }

  else if (command == "shape")
  {
    line_stream >> command;
    if (command == "sphere") { convert_sphere(line_stream); }
    if (command == "box") { convert_box(line_stream); }
    if (command == "composite") { convert_composite(line_stream); }
  }

  else if (command == "light") { convert_light(line_stream); }
  else if (command == "camera") { convert_camera(line_stream); }
}

void SDFloader::what_transform(std::stringstream& line_stream)
{
  std::string objectname;
  line_stream >> objectname;

  std::string command;
  line_stream >> command;

  if (command == "rotate") { transform_rotate(objectname, line_stream); }
  if (command == "translate") { transform_translate(objectname, line_stream); }
  if (command == "scale") { transform_scale(objectname, line_stream); }



}

/********************+++*******************
* ---- get_float, get_color, get_point ----
*********************+++*******************
* Hilfsmethoden, um aus dem Reststring
* die jeweiligen Objekte zu erzeugen.
*
* get_float wandelt den string in float um.
* get_color und get_point benutzen 
* get_float, um die gewünschten Daten
* abzugreifen und umzuwandeln
*********************+++*******************/

void SDFloader::get_float(std::stringstream& line_stream, float& f)
{
  line_stream >> f;
}

void SDFloader::get_color(std::stringstream& line_stream, Color& clr)
{
  get_float(line_stream, clr.r);
  get_float(line_stream, clr.g);
  get_float(line_stream, clr.b);
}

void SDFloader::get_point(std::stringstream& line_stream, glm::vec3& point)
{
  get_float(line_stream, point.x);
  get_float(line_stream, point.y);
  get_float(line_stream, point.z);
}


/*************************************
* --------- Convert-Methoden ---------
**************************************
* Nach der Selektierung wird hier
* eine Methode angesprochen, die
* das entsprechende Objekt erzeugt
* und an die jeweiligen Container der
* Scene übergibt.
**************************************/


void SDFloader::convert_material(std::stringstream& line_stream)
{
  std::string name;
  line_stream >> name;

  Color ambient;
  get_color(line_stream, ambient);

  Color diffuse;
  get_color(line_stream, diffuse);

  Color specular;
  get_color(line_stream, specular);

  float m;
  get_float(line_stream, m);

  Material mat{{name}, {ambient}, {diffuse}, {specular}, {m} };
  scene_.mat_.insert(std::make_pair(name, mat));
}



void SDFloader::convert_sphere(std::stringstream& line_stream)
{
  std::string name;
  line_stream >> name;

  glm::vec3 center;
  get_point(line_stream, center);

  float radius;
  get_float(line_stream, radius);

  std::string mat_name;
  line_stream >> mat_name;
  auto mat = scene_.mat_.find(mat_name) -> second;

  scene_.obj_.push_back(std::make_shared<Sphere>(name, center, radius, mat));
}



void SDFloader::convert_box(std::stringstream& line_stream)
{
  std::string name;
  line_stream >> name;

  glm::vec3 pmin;
  get_point(line_stream, pmin);

  glm::vec3 pmax;
  get_point(line_stream, pmax);

  std::string mat_name;
  line_stream >> mat_name;
  Material mat = scene_.mat_.find(mat_name) -> second;

  scene_.obj_.push_back(std::make_shared<Box>(name, pmin, pmax, mat));
}



void SDFloader::convert_composite(std::stringstream& line_stream)
{
  std::string name;
  line_stream >> name;

  Composite compy{name};

  while(line_stream.good())
  {
    std::string object;
    line_stream >> object;

    for(auto const& item : scene_.obj_)
    {
      if (item -> name() == object)
      {
        compy.add(item);
        break;
      }
    }
  }
  scene_.obj_.push_back(std::make_shared<Composite>(compy));
}



void SDFloader::convert_light(std::stringstream& line_stream)
{
 std::string name;
 line_stream >> name;

 glm::vec3 pos;
 get_point(line_stream, pos); 

 glm::vec3 la;
 get_point(line_stream, la);

 glm::vec3 ld;
 get_point(line_stream, ld);

 scene_.light_.push_back({name, pos, la, ld});
}



void SDFloader::convert_camera(std::stringstream& line_stream)
{
  std::string name;
  line_stream >> name;

  float fov_x;
  get_float(line_stream, fov_x);

  scene_.cam_ = {name, fov_x};
}


/*********************
* Transform-Methoden
**********************/

void SDFloader::transform_rotate(std::string& objectname, std::stringstream& line_stream)
{
  float angle;
  line_stream >> angle;

  // Vektor wird nicht benutzt -> überflüssig :'( ?
  glm::vec3 vector;
  get_point(line_stream, vector);

  glm::mat4x4 rotmat{ {1,0,0,0} , {0,cos(angle),sin(angle),0} , {0,-sin(angle),cos(angle),0} , {0,0,0,1} };

  rotate_.insert(std::make_pair(objectname, rotmat));
}


void SDFloader::transform_translate(std::string& objectname, std::stringstream& line_stream)
{
  glm::vec3 vector;
  get_point(line_stream, vector);

  glm::mat4x4 transmat{ {1,0,0,0} , {0,1,0,0} , {0,0,1,0} , {vector.x,vector.y,vector.z,1} };

  translate_.insert(std::make_pair(objectname, transmat));  
}


void SDFloader::transform_scale(std::string& objectname, std::stringstream& line_stream)
{
  glm::vec3 vector;
  get_point(line_stream, vector);

  glm::mat4x4 scalemat{ {vector.x,0,0,0} , {0,vector.y,0,0} , {0,0,vector.z,0} , {0,0,0,1} };

  scale_.insert(std::make_pair(objectname, scalemat));
}


void SDFloader::merge_transforms()
{
  glm::mat4x4 temptrans;
  glm::mat4x4 temprot;
  glm::mat4x4 tempscale;

  glm::mat4x4 einheitsmatrix{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}};


  for(auto const& item : scene_.obj_)
  {
    auto tempname = item -> name();

    if (translate_.find(tempname) == translate_.end()) { temptrans = einheitsmatrix; }
    else { temptrans = translate_.at(tempname); }

    if (rotate_.find(tempname) == rotate_.end()) { temprot = einheitsmatrix; }
    else { temprot = rotate_.at(tempname); }

    if (scale_.find(tempname) == scale_.end()) { tempscale = einheitsmatrix; }
    else { tempscale = scale_.at(tempname); }

    item -> set_world_transformation(temptrans * temprot * tempscale);
    item -> set_world_transformation_inv(glm::inverse(temptrans * temprot * tempscale));
  }
}


void SDFloader::print()
{
  //Materialienausgabe
  std::cout << "\n";
  std::cout << "Ausgabe aller erzeugten Materialien:\n";
  for(auto const& item : scene_.mat_)
  {
    std::cout << item.second << "\n";
  }

  // Objektausgabe
  std::cout << "Ausgabe aller " << scene_.obj_.size() << " erzeugten Objekte:\n";
  
  for(auto const& item : scene_.obj_)
  {
    item -> print(std::cout);
    std::cout << std::endl;
  }
  std::cout << "\n";

  // Lichtausgabe
  std::cout << "Ausgabe aller " << scene_.light_.size() << " erzeugten Lichter:\n";
  for(auto const& item : scene_.light_)
  {
    std::cout << item;
  }
  std::cout << "\n";

  // Kameraausgabe
  std::cout <<"Ausgabe der Kamera:\n";
  std::cout << scene_.cam_ << "\n";


}
