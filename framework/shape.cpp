#include "shape.hpp"

std::ostream& Shape::print(std::ostream& os) const
{
  os << "name: " << name_
     << "\n material: " << mat_.name()
     << "\n world trans: " << "(" << world_transformation_[0][0] << " " << world_transformation_[1][0] << " " << world_transformation_[2][0] << " " << world_transformation_[3][0] << ", "
     							<< world_transformation_[0][1] << " " << world_transformation_[1][1] << " " << world_transformation_[2][1] << " " << world_transformation_[3][1] << ", "
     							<< world_transformation_[0][2] << " " << world_transformation_[1][2] << " " << world_transformation_[2][2] << " " << world_transformation_[3][2] << ", "
     							<< world_transformation_[0][3] << " " << world_transformation_[1][3] << " " << world_transformation_[2][3] << " " << world_transformation_[3][3] << ")"

	 << "\n world trans_inv: " << "(" << world_transformation_inv_[0][0] << " " << world_transformation_inv_[1][0] << " " << world_transformation_inv_[2][0] << " " << world_transformation_inv_[3][0] << ", "
     							<< world_transformation_inv_[0][1] << " " << world_transformation_inv_[1][1] << " " << world_transformation_inv_[2][1] << " " << world_transformation_inv_[3][1] << ", "
     							<< world_transformation_inv_[0][2] << " " << world_transformation_inv_[1][2] << " " << world_transformation_inv_[2][2] << " " << world_transformation_inv_[3][2] << ", "
     							<< world_transformation_inv_[0][3] << " " << world_transformation_inv_[1][3] << " " << world_transformation_inv_[2][3] << " " << world_transformation_inv_[3][3] << ")\n";
  return os; 
}


std::ostream& operator <<(std::ostream& os, Shape const& shape)
{
  return shape.print(os);
}


std::string Shape::name() const
{
  return name_;
}

Material Shape::material() const
{
  return mat_;
}

void Shape::set_world_transformation(glm::mat4x4 const& matrix)
{
	world_transformation_ = matrix;
}

void Shape::set_world_transformation_inv(glm::mat4x4 const& matrix)
{
	world_transformation_inv_ = matrix;
}