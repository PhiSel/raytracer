#ifndef BUW_SCENE_HPP
#define BUW_SCENE_HPP 

#include <vector>
#include <map>
#include <memory>

#include "material.hpp"
#include "camera.hpp"
#include "light.hpp"
#include "shape.hpp"

class Scene
{

friend class SDFloader;
friend class Renderer;

public:
  Scene():
    filename_{"Scene"},
    width_{600},
    height_{600},
    mat_{},
    obj_{},
    light_{},
    cam_{} {}

  Scene(std::string const& filename, unsigned int width, unsigned int height, std::map<std::string, Material> const& mat, std::vector<std::shared_ptr<Shape> > const& obj, std::vector<Light> const& light, Camera const& cam):
    filename_{filename},
    width_{width},
    height_{height},
    mat_{mat},
    obj_{obj},
    light_{light},
    cam_{cam} {}

  ~Scene(){}

private:
  std::string filename_;
  unsigned int width_;
  unsigned int height_;
  std::map<std::string, Material> mat_;
  std::vector< std::shared_ptr<Shape> > obj_;
  std::vector<Light> light_;
  Camera cam_;

};

#endif //BUW_SCENE_HPP