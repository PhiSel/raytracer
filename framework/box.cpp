#include "box.hpp"

HitPoint Box::intersect(Ray const& ray) const
{
  float tmin = (pmin_.x - ray.origin.x) / ray.direction.x;
  float tmax = (pmax_.x - ray.origin.x) / ray.direction.x;

  if (tmin >= tmax) { std::swap(tmin, tmax); }

  float tymin = (pmin_.y - ray.origin.y) / ray.direction.y;
  float tymax = (pmax_.y - ray.origin.y) / ray.direction.y;

  if (tymin >= tymax)  { std::swap(tymin, tymax); }
  if ((tmin >= tymax) || (tymin >= tmax))  { return {false, 0}; }

  if (tymin >= tmin)  { tmin = tymin; }
  if (tymax < tmax)  { tmax = tymax; }

  float tzmin = (pmin_.z - ray.origin.z) / ray.direction.z;
  float tzmax = (pmax_.z - ray.origin.z) / ray.direction.z;

  if (tzmin >= tzmax)  { std::swap(tzmin, tzmax); }
  if ((tmin >= tzmax) || (tzmin >= tmax))  { return {false, 0}; }

  if (tzmin >= tmin)  { tmin = tzmin; }
  if (tzmax < tmax)  { tmax = tzmax; }

  return {true, tmin};

}

std::ostream& Box::print(std::ostream& os) const 
{
  Shape::print(os);
  os << " pmin: (" << pmin_.x << "," << pmin_.y << "," << pmin_.z << ")"
     << "\n pmax: (" << pmax_.x << "," << pmax_.y << "," << pmax_.z << ")" << "\n";
  return os; 
} 
