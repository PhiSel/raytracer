#ifndef BUW_SDFLOADER
#define BUW_SDFLOADER 

#include "material.hpp"
#include <iostream>
#include <fstream>  // using ifstream
#include <sstream>  // using stringstream 
#include <vector>
#include <map>
#include <memory>
#include <cmath>
#include <glm/glm.hpp>

#include "shape.hpp"
#include "sphere.hpp"
#include "box.hpp"
#include "composite.hpp"

#include "ray.hpp"
#include "light.hpp"
#include "camera.hpp"
#include "scene.hpp"
  
class SDFloader
{
public:
  SDFloader():
    scene_{},
    rotate_{},
    translate_{},
    scale_{} {} 
  ~SDFloader(){}
  
  // Läd eine SDF-Datei ein und übergibt classify jede Zeile
  void load(std::string const& filename);
  void print();

  Scene get_scene() const;

  std::map<std::string, Material> material() const;
  std::vector< std::shared_ptr<Shape> >object() const;
  

private:
  Scene scene_;
  std::map<std::string, glm::mat4x4>  rotate_;
  std::map<std::string, glm::mat4x4>  translate_;
  std::map<std::string, glm::mat4x4>  scale_;

  void merge_transforms();

  // Klassifiziert zwischen: define, transform, render
  void classify(std::stringstream& stream);

  // Unterscheidet zwischen den verschiedenen Definitionen und überführt sie zu den jeweiligen convert-Methoden
  void what_define(std::stringstream& stream);
  void what_transform(std::stringstream& stream);

  // Konvertierungs-Methoden für die einzelnen Fälle
  void convert_material(std::stringstream& stream);
  void convert_sphere(std::stringstream& stream);
  void convert_box(std::stringstream& stream);
  void convert_composite(std::stringstream& stream);
  void convert_light(std::stringstream& stream);
  void convert_camera(std::stringstream& stream);

  //Transformations-Methoden
  void transform_rotate(std::string& objectname, std::stringstream& line_stream);
  void transform_translate(std::string& objectname, std::stringstream& line_stream);
  void transform_scale(std::string& objectname, std::stringstream& line_stream);

  // Hilfsmethoden zum Umgang mit dem stringstream
  void get_float(std::stringstream& stream, float& f);
  void get_color(std::stringstream& stream, Color& clr);
  void get_point(std::stringstream& stream, glm::vec3& center);

};
#endif