#ifndef BUW_HITPOINT_HPP
#define BUW_HITPOINT_HPP 

#include <memory>
// #include "shape.hpp"

struct HitPoint
{
  
  bool hitSomething;
  // std::shared_ptr<Shape> shape;
  float rayParameter;


  // HitPoint(bool h, std::shared_ptr<Shape> s, float r):
  HitPoint(bool h, float r):
    hitSomething{h},
    // shape{s},
    rayParameter{r} {}
};

#endif //#ifndef BUW_HITPOINT_HPP