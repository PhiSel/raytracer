#ifndef BUW_SPHERE_HPP
#define BUW_SPHERE_HPP

#include "ray.hpp"
#include "shape.hpp"


class Sphere : public Shape
{
public:
  Sphere():
    Shape{"Sphere", {} },
    center_{0.0,0.0,0.0},
    radius_{0} {}

  Sphere(std::string const& name, glm::vec3 const& center, float radius, Material const& mat):
    Shape{name, {mat}},
    center_{center},
    radius_{radius} {}

  ~Sphere(){}

  glm::vec3 center() const;

  std::ostream& print(std::ostream& os) const override;

  HitPoint intersect(Ray const& ray) const override;


private:
  glm::vec3 center_;
  float radius_;
  Material mat_;


};

#endif //#ifndef BUW_SPHERE_HPP