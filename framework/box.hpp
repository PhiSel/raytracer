#ifndef BUW_BOX_HPP
#define BUW_BOX_HPP 

#include "shape.hpp"


class Box : public Shape
{
public:
  Box():
    Shape{"Box", {} },
    pmin_{0.0,0.0,0.0},
    pmax_{1.0,1.0,-1.0} {}

  Box(std::string const& name, glm::vec3 const& pmin, glm::vec3 const& pmax, Material const& mat):
    Shape{name, {mat}},
    pmin_{pmin},
    pmax_{pmax} {}


  ~Box(){}

  HitPoint intersect(Ray const& ray) const override;
  std::ostream& print(std::ostream& os) const;




private:
/******************************************
* pmin = vorderer unterer linker Eckpunkt *
* pmax_ = hintere oberer rechter Eckepunkt *
*******************************************/
  glm::vec3 pmin_; 
  glm::vec3 pmax_;
};

std::ostream& operator <<(std::ostream& os, Box const& box);

#endif //#ifndef BUW_BOX_HPP