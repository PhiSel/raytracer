#include "camera.hpp"


std::string Camera::name() const
{
  return name_;
}

glm::vec3 Camera::origin() const
{
  return origin_;
}

float Camera::fov_x() const
{
  return fov_x_;
}

std::pair<int, int> Camera::resolution() const
{
  return resolution_;
}


std::ostream& operator <<(std::ostream& os, Camera const& camera)
{
  os << "name: " << camera.name() 
     << "\n origin: " << "(" << camera.origin().x << "," << camera.origin().y << "," << camera.origin().z << ")"
     << "\n fov_x: " << camera.fov_x()
     << "\n resolution: " << "(" << camera.resolution().first << "," << camera.resolution().second << ")" << "\n";

  return os;
}
