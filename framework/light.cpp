#include "light.hpp"

glm::vec3 Light::get_la() const
{
  return la_;
}

glm::vec3 Light::get_ld() const
{
  return ld_;
}

std::string Light::name() const
{
  return name_;
}

glm::vec3 Light::get_pos() const
{
	return pos_;
}

std::ostream& operator <<(std::ostream& os, Light const& light)
{
  os << "name: " << light.name()
     << "\n la: " << "(" << light.get_la().x << ", " << light.get_la().y << ", " << light.get_la().z << ")"
     << "\n ld: " << "(" << light.get_ld().x << ", " << light.get_ld().y << ", " << light.get_ld().z << ")"  << "\n";
  return os; 
}