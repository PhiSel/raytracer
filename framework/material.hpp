#ifndef BUW_MATERIAL_HPP
#define BUW_MATERIAL_HPP 

#include "color.hpp"
#include <iostream>
  
class Material
{
public:
/****************
* Konstruktoren *
*****************/
  Material():
    name_{"Material"},
    ka_{0,0,0}, kd_{0,0,0}, ks_{0,0,0},
    m_{0} {}
  Material(std::string name, Color const& ka, Color const& kd, Color const& ks, float m):
    name_{name},
    ka_{ka}, kd_{kd}, ks_{ks},
    m_{m} {}

  ~Material(){}

/******************
* Getter-Methoden *
*******************/
  std::string name() const;
  Color ka() const;
  Color kd() const;
  Color ks() const;
  float m() const;

// private:
/***************************************************************************************************************
* ka_ = ambienter Reflexionskoeffizient – Anteil des ambienten Lichts, der von der Oberflaeche reflektiert wird *
* kd_ = diffuser Reflexionskoeffizient der Oberflaeche                                                          *
* ks_ = Reflexionskoeffizient für die Spiegelung                                                               *
*  m_ = Exponent für die spiegelnde Reflexion 
****************************************************************************************************************/
  std::string name_;
  Color ka_, kd_, ks_;
  float m_;
};

std::ostream& operator <<(std::ostream& os, Material const& mat);

#endif //#ifndef BUW_MATERIAL_HPP