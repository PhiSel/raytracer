#include "sphere.hpp"
#include <iostream>


glm::vec3 Sphere::center() const
{
  return center_;
}

/*********************************
* Geometrische Lösung für das Schnittpunkt-Problem.
*
* Ist effizienter als vorgestellte Methode, da frühzeitig 
* entschieden werden kann, ob überhaupt ein Schnittpunkt 
* exisistiert und dadurch unnötige Berechnungen erspart bleiben
**********************************/
HitPoint Sphere::intersect(Ray const& ray) const
{
  // Vektor vom Ursprung des Strahls zum Zentrum der Kugel
  auto l = center_ - ray.origin;   // liefert Vektor zurück
  auto l_quad = glm::dot(l, l);    // liefert float zurück 

  // Länge der Projektion von l auf den Richtungsvektor direction des Strahls
  float s = glm::dot(l,ray.direction);

  /*********************************
  * Wenn s < 0, dann liegt das Zentrum der Kugel hinter dem Ursprung des Strahles
  * Wenn l^2 < r^2, dann liegt der Ursprung des Strahls außerhalb der Kugel
  * Daraus folgt, dass es keinen Schnittpunkt gibt
  **********************************/

  if ((s < 0) && (l_quad > pow(radius_, 2)))
  {
    // std::pair<bool, float> returnvalue(false, 0);
    return {false, 0.0};//returnvalue;
  }

  // m ist kürzester Abstand des Kugelzentrums vom Strahl
  // Berechnung des quad. Abstandes des Zentrums von seiner Projektion auf den Strahl
  auto m_quad = l_quad - pow(s,2);

  /*********************************
  * Wenn m^2 > r^2, gibt es keinen Schnittpunkt (Strahl zieht an Kugel vorbei)
  **********************************/
  if (m_quad > pow(radius_,2)) 
  {
    // std::pair<bool, float> returnvalue(false, 0);
    return {false, 0.0};//returnvalue;
  }

  /*********************************
  * Ansonsten gibt es 2 Schnittpunkte:
  *   t = s - q  falls Ursprung des Strahls außerhalb der Kugel
  *   t = s + q  sonst
  **********************************/

  float q = sqrt(pow(radius_,2)-m_quad);
  if (l_quad > pow(radius_,2))
  {
    // std::pair<bool, float> returnvalue(true, s-q);
    return {true, {s-q}};//returnvalue;
    }
  else
  {
    // std::pair<bool, float> returnvalue(true, s+q);
    return {true, s+q};//returnvalue;
  }
}

std::ostream& Sphere::print(std::ostream& os) const 
{
  Shape::print(os);
  os << " center: (" << center_.x << "," << center_.y << "," << center_.z << ")\n"
     << " radius: " << radius_ << "\n";
  return os; 
} 