#ifndef BUW_RAY_HPP
#define BUW_RAY_HPP 

#include <glm/glm.hpp>

struct Ray
{
  glm::vec3 origin;
  glm::vec3 direction;

  Ray(glm::vec3 o, glm::vec3 d):
    origin{o},
    direction{d} {}
};

#endif //#ifndef BUW_RAY_HPP