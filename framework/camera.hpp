#ifndef BUW_CAMERA_HPP
#define BUW_CAMERA_HPP 

#include <glm/glm.hpp>
#include <ray.hpp>
#include <string>
#include <iostream>

class Camera
{
public:
  Camera():
    name_{"camera"},
    origin_{0,0,0},
    fov_x_{0},
    resolution_{std::make_pair(100,100)} {}

  Camera(std::string const& name, float fov_x):
    name_{name},
    origin_{0,0,0},
    fov_x_{fov_x},
    resolution_{std::make_pair(100,100)} {}

  Camera(std::string const& name, glm::vec3 const& origin, float fov_x, std::pair<int, int> const& resolution):
    name_{name},
    origin_{origin},
    fov_x_{fov_x},
    resolution_{resolution} {}

  ~Camera(){}

  Ray calc_ray(unsigned int pixelPosX, unsigned int pixelPosY);

  std::string name() const;
  glm::vec3 origin() const;
  float fov_x() const;
  std::pair<int, int> resolution() const;

private:
  std::string name_;
  glm::vec3 origin_;
  float fov_x_;
  std::pair<int,int> resolution_;
};

std::ostream& operator <<(std::ostream& os, Camera const& camera);


#endif //BUW_CAMERA_HPP