#ifndef BUW_SHAPE_HPP
#define BUW_SHAPE_HPP 

#include "material.hpp"
#include <glm/glm.hpp>
#include <cmath>
#include <iostream>
#include "ray.hpp"
#include "hitpoint.hpp"
  
class Shape
{
public:
  Shape():
    name_{"Shape"},
    mat_{},
    world_transformation_{},
    world_transformation_inv_{} {}

  Shape(std::string name, Material const& mat):
    name_{name},
    mat_{mat} {}

  virtual ~Shape(){}

  virtual std::ostream& print(std::ostream& os) const;

  virtual HitPoint intersect(Ray const& ray) const=0;

  // Getter
  std::string name() const;
  Material material() const;

  // Setter
  void set_world_transformation(glm::mat4x4 const& matrix);
  void set_world_transformation_inv(glm::mat4x4 const& matrix);


protected:
  std::string name_;
  Material mat_;
  glm::mat4x4 world_transformation_;
  glm::mat4x4 world_transformation_inv_;
};

std::ostream& operator <<(std::ostream& os, Shape const& shape);

#endif