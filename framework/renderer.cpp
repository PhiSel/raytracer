#include "renderer.hpp"
#include "ray.hpp"
#include "sphere.hpp"
#include "box.hpp"
#include <glm/glm.hpp>
#include <utility>
#include <iostream>

Renderer::Renderer(unsigned w, unsigned h, std::string const& file, Scene const& scene)
  : width_(w)
  , height_(h)
  , colorbuffer_(w*h, Color(0.0, 0.0, 0.0))
  , filename_(file)
  , ppm_(width_, height_)
  , scene_{scene}
{}

void Renderer::render()
{
  const std::size_t checkersize = 20;

  Sphere s{"first_Sphere", glm::vec3{0.0,0.0,0.0}, 0.5, {}};
  Box b{"first_Box", glm::vec3{0.0,0.0,0.0}, glm::vec3{0.5,0.5,0.5}, {} };

  for (unsigned y = 0; y < height_; ++y) {
    for (unsigned x = 0; x < width_; ++x) {
      Pixel p(x,y);

      glm::vec3 origin{float(x)/float(width_) * 2.0 -1.0, float(y)/float(height_) *2.0 -1.0, 0.0};
      glm::vec3 direction{0.1,0.1,-1.0};
      Ray r{origin, direction};

      Color rt = raytrace(r);

      p.color += rt;


      // HitPoint res = s.intersect(r); //Schnittpktbestimmung von Kugel und Strahl

      // if (res.hitSomething)
      // {
      //   p.color = Color(1.0,0.5,0.0);
      // }
      // else
      // {
      //   p.color = Color(0.0,0.0,0.0);
      // }

      // if ( ((x/checkersize)%2) != ((y/checkersize)%2)) {
      //   p.color = Color(0.0, 1.0, float(x)/height_);
      // } else {
      //   p.color = Color(1.0, 0.0, float(y)/width_);
      // }

      write(p);
    }
  }
  ppm_.save(filename_);
}

void Renderer::write(Pixel const& p)
{
  // flip pixels, because of opengl glDrawPixels
  size_t buf_pos = (width_*p.y + p.x);
  if (buf_pos >= colorbuffer_.size() || (int)buf_pos < 0) {
    std::cerr << "Fatal Error Renderer::write(Pixel p) : "
      << "pixel out of ppm_ : "
      << (int)p.x << "," << (int)p.y
      << std::endl;
  } else {
    colorbuffer_[buf_pos] = p.color;
  }

  ppm_.write(p);
}

Color Renderer::raytrace(Ray const& ray)
{
  for(auto const& item : scene_.obj_)
  {
    auto hit = item -> intersect(ray);
    if (hit.hitSomething)
    {
      auto temp = shade(hit, ray, item);
      return temp;
    }
  }
  return {0.2,0.2,0.2};
}

HitPoint Renderer::trace(Ray const& ray)
{
  for(auto const& item : scene_.obj_)
  {
    auto hit = item -> intersect(ray);
    if (hit.hitSomething)
    {
      return hit;
    }
  }
  return {false, 0};
}

Color Renderer::shade(HitPoint const& hp, Ray const& ray, std::shared_ptr<Shape> shape)
{
  glm::vec3 ov = ray.origin + hp.rayParameter * ray.direction;

  Color temp_clr{1,1,1};
  auto material = shape -> material();

  /****************************
  * l = Vektor zur Lichtquelle
  * n = Normale zur Oberfläche
  *****************************/

  for(auto const& item : scene_.light_)
  {
    glm::vec3 rv = /*glm::normalize*/(item.get_pos() - ov);
    Ray l{ov, rv};
    glm::vec3 n = /*glm::normalize*/(glm::cross(ray.direction, l.direction));

    float cosNL = (glm::dot(n, l.direction)/glm::dot(n,n)*glm::dot(l.direction, l.direction));
    // std::cout << cosNL << std::endl;
     if(cosNL < 0) {
        cosNL = -cosNL;
      }

    auto hit = trace(l);

    if (hit.hitSomething)
    {
      temp_clr = {material.kd_.r * cosNL, material.kd_.g * cosNL, material.kd_.b * cosNL};
    }
  }
  return temp_clr;
}
