#ifndef BUW_COMPOSITE_HPP
#define BUW_COMPOSITE_HPP 

#include <vector>
#include "shape.hpp"

class Composite : public Shape
{
public:
  Composite():
    Shape{"Composite", {}},
    compy_{} {}

  Composite(std::string name):
    Shape{name, {}},
    compy_{} {}

  ~Composite(){}

  void add(std::shared_ptr<Shape> const& s);
  HitPoint intersect(Ray const& ray) const override;
  std::ostream& print(std::ostream& os) const override;

private:
  std::vector< std::shared_ptr<Shape> > compy_;
};

#endif