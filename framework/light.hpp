#ifndef BUW_LIGHT_HPP
#define BUW_LIGHT_HPP 

#include <glm/glm.hpp>
#include <string>
#include <iostream>

class Light
{
public:
  Light():
    name_{"Light"},
    pos_{0.0,0.0,0.0},
    la_{0.0,0.0,0.0},
    ld_{0.0,0.0,0.0} {}

  Light(std::string const& name, glm::vec3 const& pos, glm::vec3 const& la, glm::vec3 const& ld ):
    name_{name},
    pos_{pos},
    la_{la},
    ld_{ld} {}

  ~Light(){}

  glm::vec3 get_la() const;
  glm::vec3 get_ld() const;
  glm::vec3 get_pos() const;
  std::string name() const;

private:
  std::string name_; 
  glm::vec3 pos_;
  glm::vec3 la_;
  glm::vec3 ld_;
};

std::ostream& operator <<(std::ostream& os, Light const& light);


#endif //#ifndef BUW_LIGHT_HPP